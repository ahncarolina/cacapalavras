// dados do array palavras
const min = 0;
const max = palavras.length;



//dados do display do jogo
const nRows = 10;
const nCols = 10;
const nCells = nRows * nCols;
const n = 3; // número de palavras escondidas



// variáveis globais
const displayBtn = document.getElementById('display');
const wordsList = document.getElementById('wordsList');
const resetBtn = document.getElementById('button');
const boardMsg = document.getElementById('boardMsg');
var filledDisplay = [];
var selectedWord = [];
var selectedWordsArr = [];
var hiddenWords = [];
var wins = 0;
var alreadyFilledCells = [];



// função que gera um número aleatório de [min a max[
function getRandomInt(min,max) {
    let num = Math.floor(Math.random() * (max - min) + min);
    return num;
}



// função que escolhe n palavras
function getWords(n) {
    let wordsArr = [];
    let justWords = [];
    for (let i = 0; i < n; i++) {
        let indexPal = getRandomInt(min,max);
        let word = palavras[indexPal];
        if (justWords.includes(word)){
            i = i - 1;
            console.log("repetidas :" + justWords);
        } else {
            hiddenWords.push(word);
            justWords.push(word);
            let nLetters = word.length;
            word = word.split("");

            let position = getRandomInt(0,nCells);
            let finalPosition = position + nLetters -1;
            
            let conj = [position, finalPosition, word];
            conj = conj.flat();
    
            conj = checkOverflow(conj, nLetters) // checa se palavra não ultrapassa a linha

            let checkPos = checkPosition(conj[0],conj[1]);
            // console.log("antes: " + conj[0] +" - "+ conj[1] +" - "+ alreadyFilledCells);
            while (checkPos === false) { // para não sobrescrever palavras
                // console.log("tentou sobrecrever");
                conj[0] = getRandomInt(0,nCells);
                conj[1] = conj[0] + nLetters -1;
                conj = checkOverflow(conj, nLetters); //////////////////////////////
                checkPos = checkPosition(conj[0],conj[1]);
                // console.log("depois: " + conj[0] +" - "+ conj[1] +" - "+ alreadyFilledCells);
                // console.log("dentro do while" + conj)
            }

            wordsArr.push(conj);

        }
    }
    console.log(wordsArr);
    boardHiddenList();
    return wordsArr;
}



// função que verifica se a palavra ultrapassou uma linha e vazou para a linha seguinte
function checkOverflow(conj, nLetters) {
    let pos = String(conj[0]).padStart(2, '0'); // inclui zero no início do número se ele não tiver casa decimal, ex: 01, 02
    let fpos = String(conj[1]).padStart(2, '0');
    let newPos = "";
    
    // console.log("inicio: " + pos[0]+ " - " + fpos[0])
    while (pos[0] != fpos[0]){
        // console.log("meio: " + pos[0]+ " - " + fpos[0])
        newPos = getRandomInt(0,100)
        conj[0] = newPos;
        conj[1] = newPos + nLetters -1;
        pos = String(conj[0]).padStart(2, '0');
        fpos = String(conj[1]).padStart(2, '0');
    }
    return conj;
}



// função que impede que as palavras escondidas se sobrescrevam
function checkPosition(position, finalPosition) {
    for (let i = position; i <= finalPosition; i++) {
        if (alreadyFilledCells.includes(i)) {
            for (let j = position; j < i; j++) { // apagar as posições que foram ocupadas até a tentativa de sobrescrever
                alreadyFilledCells.pop();
            }
            return false;
        } else {
            alreadyFilledCells.push(i);
        }
    }
    return true;
}



// função que escolhe uma letra do alfabeto
function getLetter() {
    let indexLetter = getRandomInt(0,alfabeto.length);
    let letter = alfabeto[indexLetter];
    return letter;
}



// função que preenche o display com letras aleatórias e as palavras escondidas
function fillRandomDisplay() {
    let wordsArr = getWords(n);
    let display = [];

    let l = "";
    let count = 0;

    for (let i = 0; i < nCells; i++) {

        if (i === wordsArr[0][0]) {
            count = 0;
            while (i != (wordsArr[0][1] + 1)){
                
                display.push(wordsArr[0][2+count]);
                count++;
                i++;
            } i--;
        } else if (i === wordsArr[1][0]) {
            count = 0;
            while (i != (wordsArr[1][1] + 1)){
                
                display.push(wordsArr[1][2+count])
                count++;
                i++;
            }i--;
        } else if (i === wordsArr[2][0]) {
            count = 0;
            while (i != (wordsArr[2][1] + 1)){
                
                display.push(wordsArr[2][2+count])
                count++;
                i++;
            }i--;
        } else {
            l = getLetter();
            display.push(l);
        }
    }
    return display;
}



// função que constroi o display na página
function construct(filledDisplay) {
    const container = document.getElementById('display');
    for (let i = 0; i < filledDisplay.length; i++) {
        let item = document.createElement('div');
        item.className = 'cell';
        item.innerHTML = filledDisplay[i];
        container.appendChild(item);
    }
}



// função que cria os blocos das palavras escondidas
function boardHiddenList() {
    for (let i = 0; i < hiddenWords.length; i++) {
        let item = document.createElement('div');
        item.innerHTML = hiddenWords[i];
        item.className = "hidden";
        wordsList.appendChild(item);

    }
}



// função que mostra o bloco da palavra encontrada
function boardShowList(w) {
    let itens = wordsList.childNodes;
    for (let i = 0; i < itens.length; i++){
        if(itens[i].innerHTML == w) {
            itens[i].classList = "shown";
        }
    }
}



// função que seleciona as células pressionadas
function selectCell(event) {
    let cellsLetters = event.target.innerHTML;
    console.log(cellsLetters);
    selectedWord.push(cellsLetters);
    event.target.classList = 'cell chosenCell';

    displayBtn.addEventListener("mouseover", selectInBetweenCells);
    displayBtn.addEventListener("mouseup", () => {
        displayBtn.removeEventListener("mouseover", selectInBetweenCells);
        displayBtn.addEventListener("mousedown", selectCell, {once: true})
        readWord();
    }, {once: true});
}

function selectInBetweenCells(event) {
    if (event.target.classList.contains('cell')){
        let cellsLetters = event.target.innerHTML;
        console.log(cellsLetters);
        selectedWord.push(cellsLetters);
        event.target.classList = 'cell chosenCell';
    }
}



// função que guarda as palavras selecionadas
function readWord() {
    console.log(selectedWord);
    let w = selectedWord.join("");
    selectedWordsArr.push(w);
    selectedWord = [];
    isValid(w);
    return w;
}



// função que valida a palavra selecionada
function isValid(w) {
    const cells = document.getElementsByClassName('chosenCell');
    if (hiddenWords.includes(w)){
        console.log("acertou - " + w);
        wins++;
        if (wins === 3) {
            sendMessage("Você encontrou " + w, false);
        } else {
            sendMessage("Você encontrou " + w, true);
        }
        checkWinner();
        setTimeout(function() {
            while (cells.length != 0) {
                cells[0].classList = "cell findedCell";
            }
            boardShowList(w);
        }, 1500);
    } else {
        console.log("errou");
        sendMessage("Você errou!", true)
        setTimeout(function() {
            while (cells.length != 0) {
                cells[0].classList = "cell";
            }
        }, 1500);
        
    }
}



// função que verifica se venceu
function checkWinner() {
    if (wins === n) {
        console.log("Parabéns! Você encontrou todas as palavras!")
        sendMessage("Parabéns! Você venceu!", false);
        displayBtn.removeEventListener("mousedown", selectCell);
    }
}



// função que reinicia o jogo
function resetGame() {
    selectedWord = [];
    selectedWordsArr = [];
    hiddenWords = [];
    wins = 0;
    alreadyFilledCells = [];

    const container = document.getElementById('display');
    let counter = container.childNodes.length;
    console.log(counter)
    while (counter != 0) {
        container.removeChild(container.lastElementChild);
        counter--;
    }

    const wList = document.getElementById('wordsList');
    let list = wList.childNodes.length;
    console.log(list)
    while (list != 0) {
        wList.removeChild(wList.lastElementChild);
        list--;
    }

    sendMessage("", true);
    
    main();
}



// função que imprime a mensagem na tela
function sendMessage(msg, check) {
    
    if (check === true) {
        boardMsg.innerHTML = msg;
        setTimeout(function() {boardMsg.innerHTML = "";}, 1500);
    } else {
        clearTimeout;
        boardMsg.innerHTML += msg + "<br>";
        
    }
}


// FUNÇÃO PRINCIPAL //////////////////////////////////////////////
window.onload = main();
function main() {
    filledDisplay = fillRandomDisplay();
    // console.log(filledDisplay);
    construct(filledDisplay);

    displayBtn.addEventListener("mousedown", selectCell, {once: true});
    
    resetBtn.addEventListener('click', resetGame);
}